import json
import os
import pickle
import socket
import threading
import urllib.request
from urllib.error import HTTPError

import certifi
import cv2
import requests
from dateutil.parser import parse
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.properties import StringProperty
from kivy.uix.screenmanager import ScreenManager, Screen
from kivymd.app import MDApp
from kivymd.uix.button import MDFlatButton
from kivymd.uix.dialog import MDDialog
from kivymd.uix.list import TwoLineListItem
from kivymd.uix.picker import MDDatePicker

os.environ['SSL_CERT_FILE'] = certifi.where()
BASE_URL = 'https://primusgrad.herokuapp.com/api/'


class LoginScreen(Screen):
	def on_enter(self, *args):
		self.child_id = StringProperty()
		self.mac_address = StringProperty()
		self.access = StringProperty()
		self.refresh = StringProperty()

		self.invalid_dialog = MDDialog(
			text="Invalid Credentials",
			buttons=[
				MDFlatButton(
					text="Ok", on_press=self.close_dialog
				),
			],
		)

	def close_dialog(self, *args):
		self.invalid_dialog.dismiss()

	def login(self, username, password):
		data = {"username": username, "password": password}
		data = urllib.parse.urlencode(data)
		data = data.encode("utf-8")

		request = urllib.request.Request(BASE_URL + "signin/", data)
		print(BASE_URL + "signin/")
		try:
			response = urllib.request.urlopen(request).read()
			data = json.loads(response)
			self.child_id = str(data["child-id"])
			self.mac_address = data["mac_address"]
			self.access = data["access"]
			self.refresh = data["refresh"]

			self.manager.add_widget(HomeScreen(name="Home"))
			self.manager.current = "Home"
		except HTTPError as e:
			print(e)
			self.invalid_dialog.open()

	def signup(self):
		self.manager.add_widget(SignUpScreen(name="SignUp"))
		self.manager.current = "SignUp"


class SignUpScreen(Screen):
	def show_date_picker(self):
		date_dialog = MDDatePicker(callback=self.get_date)
		date_dialog.open()

	def get_date(self, date):
		self.ids.date_btn.text = str(date)

	def signup(self, username, password, child_name, mac_address, date):
		print("press")
		data = {"username": username,
				"password": password,
				"child_name": child_name,
				"child_birth_date": date,
				"mac_address": mac_address}

		response = requests.post(BASE_URL + "signup/", json=data)

		print(f"Respo: {response}")
		if response.status_code == 201:
			self.manager.current = "Login"


class VideoScreen(Screen):
	def on_enter(self, *args):
		access = self.manager.screens[0].access
		mac_address = self.manager.screens[0].mac_address

		headers = {"Authorization": f"Bearer {access}"}
		data = {"mac_address": mac_address}

		response = requests.post(BASE_URL + "pi/getip/", json=data, headers=headers)

		data = response.json()
		self.ip = data["VPN_ip"]
		self.img1 = self.ids.show_img
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server = self.sock.connect((self.ip, 8080))

		self.frame = None
		self.data = b''

		self.recv = threading.Thread(target=self.recv_thread)
		self.recv.start()

	def update(self, dt):
		try:
			buf1 = cv2.flip(self.frame, 0)
			buf = buf1.tostring()
			texture1 = Texture.create(size=(self.frame.shape[1], self.frame.shape[0]), colorfmt='rgb')
			try:
				texture1.blit_buffer(buf, colorfmt='rgb', bufferfmt='ubyte')
			except Exception as e:
				print(e)
			self.img1.texture = texture1
		except:
			pass

	def hangup(self):
		self.recv.keep_running = False
		self.recv.join()
		self.sock.close()
		self.manager.current = "Home"

	def recv_thread(self):
		t = threading.current_thread()
		while getattr(t, "keep_running", True):
			size = self.sock.recv(1024)
			self.sock.sendall("start".encode('utf-8'))
			frame_data = self.sock.recv(int(size))
			while len(frame_data) < int(size):
				frame_data += self.sock.recv(int(size) - len(frame_data))

			self.frame = pickle.loads(frame_data)

			Clock.schedule_once(self.update, 0.05)


class HomeScreen(Screen):
	DATE_FMT = "%b-%d-%Y %H:%M"

	def on_enter(self, *args):
		self.temperature_list = list()
		self.pulse_list = list()

		self.child_id = self.manager.screens[0].child_id
		self.mac_address = self.manager.screens[0].mac_address
		self.access = self.manager.screens[0].access

		data = fetch_data("temp", self.child_id, self.access)
		self.update_temperature(data)

	def btn_pressed(self, pressed, refresh=False):
		if pressed == "temp":
			if refresh or not self.temperature_list:
				data = fetch_data(pressed, self.child_id, self.access)

				for i in self.temperature_list:
					self.ids.temperature_container.remove_widget(i)
				self.temperature_list.clear()

				self.update_temperature(data)

		elif pressed == "pulse_rate":
			if refresh or not self.pulse_list:
				data = fetch_data(pressed, self.child_id, self.access)

				for i in self.pulse_list:
					self.ids.pulse_container.remove_widget(i)
				self.pulse_list.clear()

				self.update_pulse(data)

		elif pressed == "video_call":
			self.manager.add_widget(VideoScreen(name="video_call"))
			self.manager.current = "video_call"

	def update_temperature(self, data):
		for i in range(len(data)):
			temp, date = data[i].split(',')
			date = parse(date).strftime(self.DATE_FMT)

			self.temperature_list.append(
				TwoLineListItem(text=temp, secondary_text=date)
			)
			self.ids.temperature_container.add_widget(self.temperature_list[i])

	def update_pulse(self, data):
		for i in range(len(data)):
			pulse, date = data[i].split(',')
			date = parse(date).strftime(self.DATE_FMT)

			self.pulse_list.append(
				TwoLineListItem(text=pulse, secondary_text=date)
			)
			self.ids.pulse_container.add_widget(self.pulse_list[i])


class PrimusApp(MDApp, ScreenManager):
	sm = ScreenManager()

	def build(self):
		self.theme_cls.theme_style = "Dark"
		self.theme_cls.primary_palette = "Cyan"
		self.sm.add_widget(LoginScreen(name="Login"))
		self.sm.current = "Login"

		return self.sm


lookup_table = {
	'temp': "child-temperature/%s",
	'pulse_rate': 'child-pulse-rate/%s',
}


def fetch_data(resource, child_id, access):
	url = BASE_URL + lookup_table[resource] % child_id
	headers = {"Authorization": f"Bearer {access}"}
	response = requests.get(url, headers=headers)

	data = response.json()
	fetched = list()
	count = 0

	try:
		for index in data:
			fetched.append(f"{index[resource]}, {index['created_at']}")
			count += 1

			if count >= 20:
				break

	except KeyError as e:
		pass

	return fetched


if __name__ == '__main__':
	PrimusApp().run()
